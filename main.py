from user import User
from post import Post

#following logic should be in another file. Class file should not contain objects. Objects should be in main.py
#import user
#importing file 'user'  and not the class 'User'
'''app_user1 = user.User("ishika.sachdev@incentius.com","Ishika Sachdev","pwd11","Business Analytics Intern")
app_user1.user.get_user_info()
app_user1.user.change_job_title("Business Analytics Trainee")
app_user1.user.change_password("Pwd1")
app_user1.user.get_user_info()'''

#to get rid of user.User
#from user import User

app_user1 = User("ishika.sachdev@incentius.com","Ishika Sachdev","pwd11","Business Analytics Intern")
app_user1.get_user_info()
app_user1.change_job_title("Business Analytics Trainee")
app_user1.change_password("Pwd1")
app_user1.get_user_info()

app_user2 = User("ishan.sachdev@incentius.com","Ishan Sachdev","pwd11","Business Analytics Intern")
app_user2.get_user_info()
app_user2.change_job_title("Business Analytics Trainee")
app_user2.change_password("Pwd1")
app_user2.get_user_info()

new_post = Post("On a secret mission today",app_user2.name)
new_post.get_post_info()
